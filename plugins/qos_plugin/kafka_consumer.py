#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#


from kafka import KafkaConsumer


def consume():
    consumer = KafkaConsumer('foobar', bootstrap_servers='172.17.0.2:9092', max_poll_records=1)
    print(list(consumer.poll(timeout_ms=1500).values())[0][0][6].decode())


if __name__ == '__main__':
    consume()
