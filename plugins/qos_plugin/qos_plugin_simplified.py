
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import json
import tornado.web
import tornado.ioloop
import urllib.parse
import random
import requests
import logging
import time
import random
logging.basicConfig(format='[%(name)s] %(asctime)s - %(message)s', level=logging.DEBUG)
module_name = "QOS_PLUGIN"
logger = logging.getLogger(module_name)


ConfigInfo = dict()
ConfigInfo['slice_id'] = "0"
ConfigInfo['register_ip'] = "http://localhost"
ConfigInfo['register_port'] = "62001"


def make_app():
    return tornado.web.Application([
        (r"/qos_plugin/get_avg_throughput_info", InfoHandler),
        (r"/cpsr-info", CPSRHandler),
    ])


class InfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        # Retrieve service info

        avg_response = elaborate_avg_data()
        print(avg_response)
        flexran_values = elaborate_current_data()
        print(flexran_values)
        response = dict()
        response['slice_id'] = ConfigInfo['slice_id']
        response['timestamp'] = int(time.time())
        response['current_values'] = flexran_values
        response['average_values'] = avg_response

        self.write(response)


class CPSRHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        logger.info(payload['register_ip'])
        ConfigInfo['slice_id'] = payload['slice_id']
        if 'http://' not in payload['register_ip']:
            ConfigInfo['register_ip'] = 'http://' + payload['register_ip']
        else:
            ConfigInfo['register_ip'] = payload['register_ip']
        ConfigInfo['register_port'] = payload['register_port']


def elaborate_current_data():
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    current_value = dict()
    entries = ["284011234500000", "284011234500001"]

    for entry in entries:
        ultbs_sum += random.randrange(1000, 2000, 150)
        dltbs_sum += random.randrange(5000, 8000, 350)
        ue.append({'imsi': entry, 'ul_throughput': random.randrange(1000, 2000, 150) / 125, 'dl_throughput': random.randrange(5000, 8000, 350) / 125})
    current_value['slice_ul_throughput'] = ultbs_sum/125
    current_value['slice_dl_throughput'] = dltbs_sum/125
    current_value['UEs'] = ue
    current_value['flows'] = {}
    return current_value


"""def elaborate_avg_data(data, slice_id):
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    avg_data = dict()
    for entry in data[slice_id]:
        ultbs_sum += entry['system.sum(ultbs)']
        dltbs_sum += entry['system.sum(dltbs)']
        ue.append({'imsi': entry['imsi'], 'avg_ul_throughput': entry['system.avg(dltbs)']/1000,
                   'avg_dl_throughput': entry["system.avg(ultbs)"]/1000})
    count = data[slice_id][0]["system.count(dltbs)"]
    # NOTE tbs is nbit per tti. tti is 1 ms. In Flexran context, ue_tbses are expessed in BYTE. To obtain Mbps: tbs x 8 x 1000 / 10000000 = tbs/125
    avg_data['slice_avg_ul_throughput'] = ultbs_sum/count/1000
    avg_data['slice_avg_dl_throughput'] = dltbs_sum / count / 1000
    avg_data['UEs'] = ue
    return avg_data
"""


def elaborate_avg_data():
    ue = []
    ultbs_sum = 0
    dltbs_sum = 0
    avg_data = dict()
    entries = ["284011234500000", "284011234500001"]
    for entry in entries:
        ultbs_sum += random.randrange(1000, 2000, 150)
        dltbs_sum += random.randrange(5000, 8000, 350)
        ue.append({'imsi': entry, 'avg_ul_throughput': random.randrange(1000, 2000, 150)/125,
                   'avg_dl_throughput': random.randrange(5000, 8000, 350)/125})
    count = 5

    # NOTE tbs is nbit per tti. tti is 1 ms. In Flexran context, ue_tbses are expessed in BYTE.
    # NOTE To obtain Mbps: tbs x 8 x 1000 / 10000000 = tbs/125
    avg_data['slice_avg_ul_throughput'] = ultbs_sum/count/125
    avg_data['slice_avg_dl_throughput'] = dltbs_sum/count/125
    avg_data['UEs'] = ue
    return avg_data


def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error", e)
        return None

    if http_response.status_code > 299:
        logger.error("GET ERROR")
        return None

    return http_response


def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Error in request", e)

    if http_response.status_code > 299:
        logger.error("error in http_response")
        raise requests.exceptions.RequestException
    return http_response


def load_api():
    with open("qos_simplified_plugin.json", "r") as f:
        json_api = json.loads(f.read())

    return json_api


def main():

    app = make_app()
    app.listen(65009)
    try:
        logger.debug('Loading API from file')
        api = load_api()
        logger.debug("Trying registration to P&P Core")
        #post('http://127.0.0.1:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        post('http://localhost:60001/plug-and-play-test/pp_management/registration/module/', data = json.dumps(api))
        logger.debug("Starting REST UI")
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    except requests.exceptions.RequestException:
        logger.error("Error in registration to P&P Core")
    except IOError as e:
        logger.error("Cannot open API file" + str(e))
    finally:
        exit(0)


if __name__ == "__main__":
    main()
