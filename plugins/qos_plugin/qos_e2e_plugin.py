#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import json
import tornado.web
import tornado.ioloop
import requests
import logging
import time
from kafka import KafkaConsumer


logging.basicConfig(format='[%(name)s] %(asctime)s - %(message)s', level=logging.DEBUG)
module_name = "QOS_E2E_PLUGIN"
logger = logging.getLogger(module_name)

# CPSR bypass
kafka_server_ip = '192.168.10.9'
kafka_server_port = '9092'
kafka_topic = 'SkyDiveStats'
consume_timeout = 3000


def make_app():
    return tornado.web.Application([
        (r"/qos_plugin/get_e2e_throughput_info", InfoHandler),
        (r"/cpsr-info", CPSRHandler),
    ])


class InfoHandler(tornado.web.RequestHandler):
    def get(self):

        consumer = KafkaConsumer('SkyDiveStats', bootstrap_servers='{}:{}'.format(kafka_server_ip, kafka_server_port),
                                 max_poll_records=1)
        message = consumer.poll(timeout_ms=consume_timeout).values()
        message = parse_kafka_message(message)
        self.write(message)


class CPSRHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        logger.debug('CPSR info received: {}:{}'.format(payload['register_ip'], payload['register_port']))


def parse_kafka_message(message):
    partial_m = list(message)
    if len(partial_m) > 0:
        message = partial_m[0][0][6].decode()
        message = json.loads(message)
    else:
        return {'Values': 'NO DATA FOUND'}

    ue = dict()
    slice_info = dict()
    enb_info = dict()
    throughput_info = dict()
    new_message = dict()

    message = message['Values'][0]

    ue['imsi'] = message['imsi']
    ue['dlSliceId'] = message['dlSliceId']
    ue['ulSliceId'] = message['ulSliceId']

    slice_info['core_slice_id'] = message['core_slice_id']
    slice_info['dlPercentage'] = message['dlPercentage']
    slice_info['ulPercentage'] = message['ulPercentage']

    enb_info['eNB_id'] = message['eNB_id']
    enb_info['dl_freq'] = message['dl_freq']
    enb_info['ul_freq'] = message['ul_freq']
    enb_info['band'] = message['band']
    enb_info['dlPercentage'] = message['dlPercentage']
    enb_info['ulPercentage'] = message['ulPercentage']

    throughput_info['upload'] = message['ABthr']
    throughput_info['download'] = message['BAthr']
    if message['RTT'] == 'null' or int(message['RTT']) > 1000000000:
        return {'Values': 'NO DATA FOUND'}
    throughput_info['rtt'] = message['RTT']

    new_message['ue'] = ue
    new_message['slice_info'] = slice_info
    new_message['enb_info'] = enb_info
    new_message['throughput_info'] = throughput_info

    return new_message


def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error", e)
        return None

    if http_response.status_code > 299:
        logger.error("GET ERROR")
        return None

    return http_response


def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Error in request", e)

    if http_response.status_code > 299:
        logger.error("error in http_response")
        raise requests.exceptions.RequestException
    return http_response


def load_api():
    with open("qos_e2e_plugin.json", "r") as f:
        json_api = json.loads(f.read())

    return json_api


def main():
    app = make_app()
    app.listen(65010)
    try:
        logger.debug('Loading API from file')
        api = load_api()
        logger.debug("Trying registration to P&P Core")
        # post('http://127.0.0.1:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        post('http://localhost:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        logger.debug("Starting REST UI")
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    except requests.exceptions.RequestException:
        logger.error("Error in registration to P&P Core")
    except IOError as e:
        logger.error("Cannot open API file" + str(e))
    finally:
        exit(0)


if __name__ == "__main__":
    main()
