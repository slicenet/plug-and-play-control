
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import json
import tornado.web
import tornado.ioloop
import urllib.parse
import random
import requests
import logging
import time

logging.basicConfig(format='[%(name)s] %(asctime)s - %(message)s', level=logging.DEBUG)
module_name = "QOS_PLUGIN_TEI"
logger = logging.getLogger(module_name)


ConfigInfo = dict()
ConfigInfo['slice_id'] = "0"
ConfigInfo['register_ip'] = "http://localhost"
ConfigInfo['register_port'] = "62001"


def make_app():
    return tornado.web.Application([
        (r"/qos_plugin_2/get_qos_cp_info", InfoHandler),
        (r"/cpsr-info", CPSRHandler),
    ])


class InfoHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        # Retrieve service info
        url = get_service_info('QOS-CP')
        logger.info('INFO_HANDLER_URL: ' + url)
        response = get_qos_cps_info(url)
        logger.info('Response type: ' + response)
        self.write(response)


class CPSRHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        logger.info(payload['register_ip'])
        ConfigInfo['slice_id'] = payload['slice_id']
        if 'http://' not in payload['register_ip']:
            ConfigInfo['register_ip'] = 'http://' + payload['register_ip']
        else:
            ConfigInfo['register_ip'] = payload['register_ip']
        ConfigInfo['register_port'] = payload['register_port']


def get_service_info(cps_type):
    url = ConfigInfo['register_ip'] + ':' + ConfigInfo[
        'register_port'] + '/slicenet/ctrlplane/cpsr_cps/v1/cps-instances?CPS-Type=' + \
          cps_type
    logger.info('URL: ' + url)
    response = get(url=url)
    if response is not None:
        got_url = json.loads(response.text)[0]['uri']
        if 'http://' not in got_url:
            got_url = 'http://' + got_url
        return got_url
    else:
        logger.error('ERROR in retrieving information from CPSR at ' + ConfigInfo['register_ip'] + ":"
                     + ConfigInfo['register_port'])
        return None


def get_qos_cps_info(base_url):
    url = base_url + '/qos-instance'
    response = get(url=url)
    return response.text


def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error", e)
        return None

    if http_response.status_code > 299:
        logger.error("GET ERROR")
        return None

    return http_response


def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Error in request", e)

    if http_response.status_code > 299:
        logger.error("error in http_response")
        raise requests.exceptions.RequestException
    return http_response


def load_api():
    with open("qos_plugin_2.json", "r") as f:
        json_api = json.loads(f.read())

    return json_api


def main():
    app = make_app()
    app.listen(65008)
    try:
        logger.debug('Loading API from file')
        api = load_api()
        logger.debug("Trying registration to P&P Core")
        #post('http://127.0.0.1:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        post('http://localhost:60001/plug-and-play-test/pp_management/registration/module/', data = json.dumps(api))
        logger.debug("Starting REST UI")
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    except requests.exceptions.RequestException:
        logger.error("Error in registration to P&P Core")
    except IOError as e:
        logger.error("Cannot open API file" + str(e))
    finally:
        exit(0)


if __name__ == "__main__":
    main()
