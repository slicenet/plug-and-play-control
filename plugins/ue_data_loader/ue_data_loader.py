
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import json
import tornado.web
import tornado.ioloop
import requests
import logging
from kafka import KafkaProducer


logging.basicConfig(format='[%(name)s] %(asctime)s - %(message)s', level=logging.DEBUG)
module_name = "UE_DATA_LOADER_PLUGIN"
logger = logging.getLogger(module_name)


ConfigInfo = {}
ConfigInfo['service_base_url'] = ""
ConfigInfo['service_endpoint'] = "/config/"
ConfigInfo['slice_id'] = ""
ConfigInfo['CPS_Type'] = "NCONF-CP"
ConfigInfo['register_ip'] = ""
ConfigInfo['register_port'] = ""

# CPSR Bypass
kafka_server_ip = '10.202.11.25'
kafka_server_port = '9092'
kafka_topic = 'ANOMALY'

def make_app():
    return tornado.web.Application([
        (r"/monitoring/", ConfigurationHandler),
        (r"/cpsr-info", CPSRHandler),
    ])


payload = None


class ConfigurationHandler(tornado.web.RequestHandler):

    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        print('POST')
        global payload
        payload = tornado.escape.json_decode(self.request.body)
        print(payload)
        produce(message=payload)
        #post(url=ConfigInfo['service_base_url'] + ConfigInfo['service_endpoint']+node_name, data=json.dumps(payload))


class CPSRHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        payload = tornado.escape.json_decode(self.request.body)
        logger.info(payload['register_ip'])
        #
        # NO CONTAINER REGISTRY DEFINED
        #
        # ConfigInfo['slice_id'] = payload['slice_id']
        # if 'http://' not in payload['register_ip']:
        #     ConfigInfo['register_ip'] = 'http://' + payload['register_ip']
        # else:
        #     ConfigInfo['register_ip'] = payload['register_ip']
        # ConfigInfo['register_port'] = payload['register_port']
        #
        # url = ConfigInfo['register_ip'] + ':' + ConfigInfo[
        #     'register_port'] + '/ctrlplane/cpsr/v1/cps-instances?CPS-Type=' + \
        #       ConfigInfo['CPS_Type'] + '&sliceId=' + payload['slice_id']
        # logger.info('URL: ' + url)
        # response = get(url=url)
        # if response is not None:
        #     ConfigInfo['service_base_url'] = json.loads(response.text)['cpss'][0]['uri']
        #     if 'http://' not in ConfigInfo['service_base_url']:
        #         ConfigInfo['service_base_url'] = 'http://' + ConfigInfo['service_base_url']
        # else:
        #     logger.error('ERROR in retrieving information from CPSR at ' + payload['register_ip'] + ":"
        #                  + payload['register_port'])
        #     self.set_status(404, "NOT FOUND")


def on_send_success(record_metadata):
    print(record_metadata.topic)
    print(record_metadata.partition)
    print(record_metadata.offset)


def on_send_error(excp):
    print('Something wrong: {}'.format(excp))


def produce(message):
    # dict to string
    message = json.dumps(message)

    producer = KafkaProducer(bootstrap_servers='{}:{}'.format(kafka_server_ip, kafka_server_port))

    producer.send(kafka_topic, message.encode()).add_callback(on_send_success).add_errback(on_send_error)


def load_api():
    with open("ue_data_loader.json", "r") as f:
        json_api = json.loads(f.read())

    return json_api


def post(url, data=None, headers=None, user=None, password=None):
    auth = None
    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)
    try:
        http_response = requests.post(url, data, auth=auth, headers=headers)
    except requests.exceptions.RequestException as e:
        logger.error("Error in request", e)

    if http_response.status_code > 299:
        logger.error("error in http_response")
        raise requests.exceptions.RequestException
    return http_response


def get(url, headers=None,  user=None, password=None, params=None):
    auth = None

    if user is not None and password is not None:
        auth = requests.auth.HTTPBasicAuth(user, password)

    try:
        http_response = requests.request("GET", url, headers=headers, auth=auth, params=params)
    except requests.exceptions.RequestException as e:
        logger.error("Connection Error", e)
        return None

    if http_response.status_code > 299:
        logger.error("GET ERROR")
        return None

    return http_response


def main():
    app = make_app()
    app.listen(65010)
    try:
        logger.debug('Loading API from file')
        api = load_api()
        logger.debug("Trying registration to P&P Core")
        #post('http://127.0.0.1:60001/plug-and-play-test/pp_management/registration/module/', data=json.dumps(api))
        post('http://localhost:60001/plug-and-play-test/pp_management/registration/module/', data = json.dumps(api))
        logger.debug("Starting REST UI")
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        logger.error("Keyboard Interrupt caught: closing.")
    except requests.exceptions.RequestException:
        logger.error("Error in registration to P&P Core")
    except IOError as e:
        logger.error("Cannot open API file" + str(e))
    finally:
        exit(0)


if __name__ == "__main__":
    main()
