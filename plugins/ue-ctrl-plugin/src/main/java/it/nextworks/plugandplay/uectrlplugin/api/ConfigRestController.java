package it.nextworks.plugandplay.uectrlplugin.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.plugandplay.uectrlplugin.engine.PluginEngine;
import it.nextworks.plugandplay.uectrlplugin.model.PluginConfig;

@CrossOrigin
@RestController
@RequestMapping(value="/plugin/management")
public class ConfigRestController {

	private Logger log = LoggerFactory.getLogger(ConfigRestController.class);
	
	@Autowired
	private PluginEngine pluginEngine;
	
	@RequestMapping(value="/config", method=RequestMethod.POST)
	@ApiOperation(value = "setConfig", nickname = "confPlugin")
	@ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created"),
        @ApiResponse(code = 400, message = "Bad Request")}) 
	public ResponseEntity<String> setConfig(@RequestBody PluginConfig config) throws Exception {

		log.debug("Received new plugin configuration");

		try {
			
			pluginEngine.setConfig(config);
			
		} catch (Exception e) {
			log.error("Cannot set new configuration: " + e.getMessage());
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("", HttpStatus.CREATED);
	}
	
}