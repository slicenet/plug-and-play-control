

#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

# P&P Control - Exceptions and Logger

import logging

logging.basicConfig(filename="plug_and_play.log", format='[%(name)s] %(asctime)s - %(message)s', filemode='w',
                    level=logging.DEBUG)


class PlugAndPlayError(Exception):
    def __init__(self, reason, etype='GENERIC_ERROR'):
        self.etype = etype
        self.reason = reason


# ERRORS:
# plugin_registration
#   reasons: Error in parsing API definition
class PluginError(PlugAndPlayError):
    def __init__(self, reason, etype):
        super().__init__(reason, etype)


# retrieve module/slice/detailed_slice_info
class GetInfoError(PlugAndPlayError):
    def __init__(self, reason, etype):
        super().__init__(reason, etype)

