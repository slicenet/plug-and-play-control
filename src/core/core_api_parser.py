
#  Copyright (c) 2019 Nextworks s.r.l.
#  Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License
#

import swagger_parser
import logging
import core_exceptions as exp
import json

module_name = "P&P_CORE_PARSER"
logger = logging.getLogger(module_name)


class Parser2:
    def __init__(self, api):
        self.api = api
        try:
            self.parser = swagger_parser.SwaggerParser(swagger_dict=api)
        except ValueError:
            raise

    def get_module_id(self):
        try:
            return self.api['info']['title']
        except KeyError:
            raise

    def get_operations(self):
        return self.parser.operation

    def get_base_path(self):
        return self.parser.base_path

    def get_spec_by_path(self, path, action):
        return self.parser.get_path_spec(path, action)

    def add_schema_to_params(self, parameters):
        try:
            for param_name, values in parameters.items():
                if values['in'] == 'body':
                    def_name = self.parser.get_definition_name_from_ref(values['schema']['$ref'])
                    del values['schema']['$ref']
                    values['schema'] = self.api['definitions'][def_name]
        except KeyError:
            raise
        return parameters


class Parser3:
    def __init__(self, api):
        self.api = api

    def get_module_id(self):
        try:
            return self.api['info']['title']
        except KeyError:
            raise


def path_fixer(base_path, path):
    if base_path[-1] == '/':
        full_path = base_path + path[1:]
    elif base_path[-1] != '/':
        full_path = base_path + path
    return full_path


def path_fixer_2(path, host):
    if len(path) > 1:
        if path[0] == '/' and path[1] == '/':
            path = path[1:]
        if 'localhost' in path:
            path = 'http:/' + path
        elif 'localhost:' in host:
            path = 'http://' + host + path
    return path


def test_engine_function3(api):
    res = {}
    try:
        parser = Parser3(api)
        mod_id = parser.get_module_id()

        res[mod_id] = {}
        res[mod_id]["base_path"] = api['servers'][0]['url']
        res[mod_id]['operations'] = []
        for path, op_type in api['paths'].items():
            for key, value in op_type.items():
                aux = dict()
                operation_id = value['operationId']
                aux[operation_id] = {}
                if 'requestBody' in value.keys():
                    schema_name = value['requestBody']['content']['application/json']['schema']['$ref'].split('/')[-1]
                    del value['requestBody']['content']['application/json']['schema']['$ref']
                    value['requestBody']['content']['application/json']['schema'] = api['components']['schemas'][schema_name]
                    aux[operation_id]['requestBody'] = value['requestBody']
                aux[operation_id]['type'] = key
                aux[operation_id]['parameters'] = []
                if 'parameters' in value.keys():

                    for par in value['parameters']:
                        aux[operation_id]['parameters'].append({par['name']: par})

                aux[operation_id]['responses'] = value['responses']
                aux[operation_id]['full_path'] = path_fixer(res[mod_id]["base_path"], path)
                res[mod_id]['operations'].append(aux)
    except KeyError:
        raise exp.PluginError(etype='Registration Error', reason='Error in parsing OpenAPI module')
    return res


def test_engine_function2(api):
    res = {}
    try:
        parser = Parser2(api)
        mod_id = parser.get_module_id()
        res[mod_id] = {}
        res[mod_id]["base_path"] = parser.get_base_path()
        res[mod_id]['operations'] = []
        operations = parser.get_operations()
        if 'host' in api.keys():
            host = api['host']
        else:
            host = ''
        for op_id, value_tuple in operations.items():
            aux = dict()
            aux[op_id] = {}
            path_tuple = parser.get_spec_by_path(value_tuple[0], value_tuple[1])
            aux[op_id]['full_path'] = path_fixer_2(path_tuple[0], host)
            aux[op_id]['type'] = value_tuple[1]
            aux[op_id]['parameters'] = []
            aux[op_id]['parameters'].append(parser.add_schema_to_params(path_tuple[1]['parameters']))
            aux[op_id]['responses'] = path_tuple[1]['responses']
            res[mod_id]['operations'].append(aux)
    except (ValueError, KeyError):
        raise exp.PluginError(etype='Registration Error', reason='Error in parsing Swagger API module')
    return res


def parse_module_api(api):
    try:
        if 'openapi' in api.keys():
            parsed_res = test_engine_function3(api)
        elif 'swagger' in api.keys():
            parsed_res = test_engine_function2(api)
        else:
            raise exp.PluginError(etype='Registration Error', reason='Error in parsing Swagger API module')
    except exp.PluginError as e:
        logger.error('Error Type: ' + e.etype + ' - Reason: ' + e.reason)
        raise
    return parsed_res

